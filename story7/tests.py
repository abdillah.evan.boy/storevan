from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from . import views

class Story7TestCase(TestCase):
    def test_ada_page(self):
        response = Client().get('/story7/')
        self.assertEqual(response.status_code, 200)
    def test_template_digunakan(self):
        response = Client().get('/story7/')
        self.assertTemplateUsed(response, 'story7.html')
    def test_fungsi_views(self):
        found = resolve('/story7/') 
        self.assertEqual(found.func, views.story7)       