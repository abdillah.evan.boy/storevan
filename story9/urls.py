from django.urls import path
from django.contrib.auth import views as av
from .views import register

app_name = 'story9'

urlpatterns = [
    path('login/', av.LoginView.as_view(template_name='auth.html'), name='auth'),
    path('logout/', av.LogoutView.as_view(template_name='logout.html'), name='logout'),
    path('register/', register, name='register'),
]
