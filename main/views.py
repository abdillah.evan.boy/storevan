from django.shortcuts import render


def home(request):
    response = {}
    return render(request, 'main/home.html', response)

def landingPage(request):
    response = {}
    return render(request, 'main/LandingPage.html', response)
